jQuery(function ($) {

  $('#screenshots td:first-child').each(function () {
    $(this).attr('title', $(this).text());
  });

  $('#screenshots').change(function () {
    var text = $(this).find('tr:has(:checked)').map(function () {
      var fix = $(this).data('fix');
      return fix.replace('curl ', 'curl ' + location.origin);
    }).get().join('\n\n');

    $('#fixes').text(text);
  });

  $('#copy').click(function () {
    var textarea = $('<textarea></textarea>');
    textarea.text($('#fixes').text() + '\n');
    $('body').append(textarea);
    textarea.select();
    document.execCommand('copy');
    textarea.remove();
  });

  var from;
  $('#screenshots :checkbox').click(function (e) {
    if (!from || !e.shiftKey) {
      from = this;
      return;
    }

    var checkbox = $('#screenshots :checkbox');
    var c12 = [checkbox.index(from), checkbox.index(this)];
    if (c12[0] > c12[1]) {
      c12.reverse();
    }
    var checked = $(this).prop('checked');
    checkbox.slice(c12[0], c12[1] + 1).prop('checked', checked);
  });

});
