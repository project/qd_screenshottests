/*
This module allows a specific database to be loaded instead of the default.
This is useful if one needs to test on an older version.

An absolute path must be provided as the argument and must end in either ".sql" or ".sql.gz".
Example:
  require('modules/load-sql')('/var/www/html/web/profiles/oequasta_profile/tests/PhantomJS/dbReports.sql');

If no argument is given, but the module has been required, a warning will be printed and the default profile database used.

*/

var profile = casper.cli.get('install-profile');

module.exports = function (database) {
  if (!database) {
    casper.echo(
      '[warning] [casper] No database specified, but load-sql module used.' + '\n' +
      'To use a specific database, please supply an argument for load-sql.' + '\n' +
      'Reverting to default database for ' + profile + '...');
  }
};
