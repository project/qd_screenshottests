/**
 * Examples:
 *   var drush = require('modules/drush');
 *   drush('pm:enable views_ui --quiet');
 *   drush('watchdog:show --type=php', true);
 *
 * @param {string|array} command
 *   Drush command to execute
 * @param {boolean} expect_stdout
 *   Optional; If expect_stdout is omitted or false, then every output is
 *   considered an error. You may supply --quiet within command if needed.
 */
module.exports = function (command, expect_stdout) {
  var drush_finished = false;
  var command_str;

  casper.then(function () {
    var args_array;
    if (typeof command == 'string') {
      command_str = command + ' -y';
      args_array = command_str.split(' ');
    }
    else {
      command.push('-y');
      command_str = command.join(' ');
      args_array = command;
    }
    this.log('Starting drush ' + command_str + '...', 'debug');
    var system = require('system');
    var drush = system.env['DRUSH'];
    var proc = require('child_process').spawn(drush, args_array);

    proc.on('exit', function (err) {
      drush_finished = true;
      if (!err) {
        return;
      }
      casper.echo('[error] [drush] ' + command_str + ' returned ' + err, 'WARNING');
      casper.exit(1);
    });

    function log_and_exit(data) {
      casper.echo('[error] [drush] ' + data, 'WARNING');
      casper.exit(1);
    }

    function log_output(data) {
      casper.echo('[info] [drush]', 'INFO');
      casper.echo(data, 'INFO');
    }

    proc.stdout.on('data', expect_stdout ? log_output : log_and_exit);
    proc.stderr.on('data', log_and_exit);
  });

  casper.waitFor(function () {
    return drush_finished;
  }, null, function () {
    casper.echo('[error] [drush] command ' + command_str
      + ' did not finish in 60s', 'ERROR');
    casper.exit(1);
  }, 60e3);
};
