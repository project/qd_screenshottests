/*
This module allows to skip a script if the current profile doesn't match at
least one passed as an argument.
This is useful if you use the same test suite for multiple web sites.

Example:
  require('modules/exert-profiles')('merck_profile', <more profiles>);

The exert-profiles call cannot be multi-line, because grep is used to pre-parse it.

To tell PhantomJS which profile the test is for, call it like this:
  $ casperjs test your_file.js --install-profile=demo_umami

*/

var profile = casper.cli.get('install-profile');

module.exports = function () {
  if (!profile && arguments.length) {
    casper.echo(
      '[warning] [casper] No profile supplied on the command line, but exert-profiles module used. ' +
      'You may want to supply the parameter --install-profile=' + arguments[0] + " to silence this warning.");
    return;
  }

  if ([].slice.apply(arguments).indexOf(profile) !== -1) {
    return;
  }
  casper.echo('[info] [casper] Ignoring test for --install-profile=' + profile, 'PARAMETER');
  casper.exit();
};
