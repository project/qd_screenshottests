<?php

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\InfoParser;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/** @var UserInterface $admin */
$admin = User::load(1);
Drupal::currentUser()->setAccount($admin);

$module_handler = Drupal::moduleHandler();

// Build a dependency tree so we can later call the hooks in the right order:
$info_parser = new InfoParser();
$extensions = array_map(
  static function (Extension $extension) use ($info_parser) {
    $extension->info = $info_parser->parse($extension->getPathname());
    return $extension;
  },
  $module_handler->getModuleList());
$extensions = $module_handler->buildModuleDependencies($extensions);

// Sort hook implementations by module dependencies:
$module_handler->loadAllIncludes('install');
$implementations = $module_handler->getImplementations('demo_content_for_screenshottest');
usort($implementations, static function ($a, $b) use ($extensions) {
  return $extensions[$b]->sort <=> $extensions[$a]->sort;
});

// Call the hooks:
foreach ($implementations as $module) {
  $module_handler->invoke($module, 'demo_content_for_screenshottest');
}
