#!/usr/bin/env bash
function _screenshottest() {
  local cur="${COMP_WORDS[COMP_CWORD]}"

  if ! [[ ${cur} == -* ]]; then
    # Doesn't start with a hyphen
    compopt -o filenames
    COMPREPLY=( $(compgen -f -o plusdirs -- $cur) )
    return
  fi

  local opts="
    $(screenshottest --help | grep -Eoe '  --[^ =]*=?' | grep -v install-profile)
    $(screenshottest --install-profile= | grep '  --')"

  COMPREPLY=( $(compgen -W "${opts}" -- $cur) )
  if [[ $COMPREPLY == *install-profile* ]]; then
    # Auto-complete Drupal install profiles (they are not files)
    compopt -o filenames
  fi
  if [[ ${#COMPREPLY[@]} == 1 && $COMPREPLY == *= ]]; then
    # There is only one suggestion and it ends with "="
    compopt -o nospace
  fi
}
complete -F _screenshottest screenshottest
