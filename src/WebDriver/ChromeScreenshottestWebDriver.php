<?php


namespace Drupal\qd_screenshottests\WebDriver;


use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeDriverService;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;

/**
 * Class ChromeScreenshottestWebDriver
 *
 * Gets run with xvfb-run to simulate a graphical environment
 *
 * @package Drupal\qd_screenshottests\WebDriver
 */
class ChromeScreenshottestWebDriver extends ChromeDriver {

  use ScreenshottestWebDriverTrait;

  public static function start(DesiredCapabilities $desired_capabilities = NULL, ChromeDriverService $service = NULL) {
    $modulePath = \Drupal::moduleHandler()
      ->getModule('qd_screenshottests')->getPath();
    $chromeDriverScript = DRUPAL_ROOT . '/' . $modulePath . '/bash/start_chromedriver';
    putenv(ChromeDriverService::CHROME_DRIVER_EXECUTABLE . '=' . $chromeDriverScript);

    if (!$desired_capabilities) {
      $options = new ChromeOptions();
      $options->addArguments(['--headless', '--no-sandbox']);
      $desired_capabilities = DesiredCapabilities::chrome();
      $desired_capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $options);
    }

    $driver = parent::start($desired_capabilities, $service);
    drupal_register_shutdown_function(static function () use ($driver) {
      $driver->quit();
    });
    return $driver;
  }

}
