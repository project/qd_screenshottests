/**
 * @param {string} local_filename
 *   A file produced by the JS test.
 * @param {string} expected
 *   The expected file content.
 */
module.exports = function (local_filename, expected) {
  var fs = require('fs');

  casper.then(function () {
    var actual = fs.read(local_filename, 'latin1');

    // Make sure that the next test fails if it doesn't produce its own file.
    fs.remove(local_filename);

    if (expected === actual) {
      casper.echo('[debug] [casper] ' + local_filename + ' perfectly matches expectations.', 'DEBUG');
      return;
    }
    casper.echo('[error] [casper] The file ' + local_filename + " (>) doesn't match expectations (<).", "ERROR");

    // Write both files to avoid encoding issues, e.g. latin1 versus utf-8:
    fs.write('/tmp/expected', expected, 'w');
    fs.write('/tmp/actual', actual, 'w');

    var diff_proc = require('child_process').spawn('diff', ['/tmp/expected', '/tmp/actual']);
    diff_proc.on('exit', function (err) {
      casper.exit(1);
    });
    diff_proc.stdout.on('data', function (data) {
      casper.echo(data, 'ERROR');
    });
    diff_proc.stderr.on('data', function (data) {
      casper.echo('!! ' + data, 'ERROR');
    });
  });
};
