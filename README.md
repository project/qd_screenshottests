Screenshot tests with CasperJS or Selenium
=====================================


Allows to make and visually compare screenshots of your Drupal website. These
can be integrated in your continuous integration environment.

This module provides two aspects for a common purpose:
- A web interface to compare screenshots.
- A CLI interface to run tests and create screenshots.

Tests are written as [CasperJS](http://casperjs.org) or [php-webdriver – Selenium](https://github.com/php-webdriver/php-webdriver) scripts.

REQUIREMENTS
------------
- Mandatory: An executable `python` needs to be on the PATH.
  It can be either Python 2 or 3.

- Optional: If you want to use the `--email` option, then the executable `mail`
  from GNU Mailutils e.g. 2.99.99* is required. In Debian/Ubuntu, you can use
  this:

  ```bash
  $ sudo apt-get install mailutils
  ```

  There are many tools called `mail`, with different accepted CLI parameters.
  Please make sure yours is compatible with Mailutils.

  You can install Postfix in addition to mailutils to allow SMTP transmission to
  your mail server.

INSTALLATION
------------
1. Add the module in your preferred way, for example
   - `composer require drupal/qd_screenshottests`
     or
   - `git clone` the repo into your Drupal modules directory
     (typically /var/www/html/web/modules/contrib)

2. There should be a new folder *qd_screenshottests*;
   switch to the folder and type

   ```bash
   $ make && sudo make install
   ```

   This will also install ImageMagick.

3. Optionally, install the Drupal module from the /admin/modules URL
   or by typing

    ```bash
    $ drush pm:enable qd_screenshottests
    ```

# Write and run screenshot tests

In contrast to QuoData's earlier screenshot test suites, changes to the database
induced by a previous do not transcend to the next test. Instead, they are
discarded before each test. Thus, tests are executed in arbitrary order.

1. Switch to a Drupal module folder
2. Add a folder _tests_. Only .js files directly within the _tests_ folder are
   considered for this Drupal module.
3. Add the following **CasperJS** test script _tests/example_test_script.js_:

    ````js
    require('modules/start')('participant');
    var capture = require('modules/capture');
    capture('first_screenshot');    
    ````

  Or add the following **Selenium** php-webdriver test _tests/example.selenium.php_:


    <?php
    // ('modules/exert-profiles')('some_install_profile');
    use Drupal\qd_screenshottests\WebDriver\ScreenshottestWebDriver;
    
    $driver = ScreenshottestWebDriver::start();
    $driver->login('admin', 'password');    
    $driver->capture('test');  


4. Run it with

    ```bash
    $ screenshottest tests/example_test_script.js
    ```
    
    Before _example_test_script.js_ is executed, screenshottest sets up a test 
    database with some demo content. When the test is completed, the previous 
    database is restored.
    
    For more information about the `screenshottest` bash script, execute
    
    ```bash
    $ screenshottest --help
    ```

5. Git and .gitignore

    #### Commit the _old_ folder
    When using this module for your own projects, you should commit all files in
    the tests/old directory of each Drupal module into source control. This
    directory contains the valuable target states.

    #### Add the _new_ and _diff_ folders to your .gitignore
    In addition, there are two additional directories that don't belong into
    source control. Therefore we recommend adding these lines to your
    `.gitignore` file:

    ```
    # Ignore intermediate files of qd_screenshottests:
    /**/tests/new/
    /**/tests/diff/
    ```

# Compare and manage screenshots

* Go to http://your-ip-or-domain/admin/config/development/screenshottests
* Or navigate to _Administration_ -> _Configuration_ -> _Development_
  -> _Screenshot test results_

# Document code coverage in .routing.yml files

Running `screenshottest --record-coverage` produces a coverage dir in the Drupal files
directory, typically _sites/default/files/screenshottest_coverage_. It
stores which routes were accessed by which test under which Drupal profile.

To make this information more accessible, you can use the following command:
```bash
$ drush qd_screenshottests:coverage-to-routing [directory]
```
Here, the optional parameter `directory` defines which .routing.yml 
files to modify. If omitted, it defaults to current directory. The search covers
all sub-directories as well.

This takes the _screenshottest_coverage_ and adds the entries to all the
.routing.yml files of our modules. The 'qd_screenshottests' key added by the Drush
command is not used by code. It is only meant for humans. Having it as YAML
instead of comments ensures that it stays in place.
Example:

```yml
entity.user.collection:
  path: /admin/people
  defaults:
    _entity_list: user
    _title: People
  requirements:
    _permission: 'administer users'
  qd_screenshottests:
    qd_user_listbuilder/tests/user.js:
      - ptop_basis_profile
```

The coverage dir survives across test runs. If you modify tests you need to
delete the coverage dir by running
```bash
drush qd_screenshottests:delete-coverage-dir
```

# See also
- https://applitools.com/
- https://cypress.io
- https://garris.github.io/BackstopJS/
- https://github.com/AppraiseQA/appraise
- https://github.com/BBC-News/wraith
- https://github.com/gemini-testing/hermione
- https://github.com/mojoaxel/awesome-regression-testing
- https://github.com/QuoData-Quality-and-Statistics/DelphiScreenshotTestsuite
- https://github.com/Sereda-Fazen/VisualCeption
- https://github.com/wearefriday/spectre
- https://percy.io/
- https://www.drupal.org/project/behat_ui
- https://www.drupal.org/project/casperjs

MAINTAINERS
-----------
Huge thanks to current and previous QuoData colleagues!

The Drupal.org user [Gogowitsch](https://www.drupal.org/u/gogowitsch) formally
released the module, but is only one of many contributors.
