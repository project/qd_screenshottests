<?php

namespace Drupal\qd_screenshottests\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\qd_button_helper\AccessAwareButton;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ManageScreenshots extends ControllerBase {

  /**
   * Loads the change array for one module instead of all, e.g.:
   * ["module" => "some_module","path" => "some/path","screenshots" => [...]]
   *
   * @param $module_name string
   *
   * @return array
   */
  public function getModuleByName($module_name) {
    $modules = $this->modules($module_name);
    return $this->getArrayElementWithKeyValue($modules, $module_name, 'module');
  }

  private function getModulePath($module_name) {
    try {
      $path = $module_name ?
        \Drupal::moduleHandler()->getModule($module_name)->getPath() :
        ".";
    } catch (UnknownExtensionException $e) {
      exec("find -name $module_name.info.yml", $aOutput, $iErrorLevel);
      if ($iErrorLevel)
        throw $e;
      $path = dirname($aOutput[0]);
    }
    return $path;
  }

  protected function modules($module_name = NULL): array {
    $aCache = \Drupal::state()->get('screenshots');
    if (!empty($aCache['expire']) && $aCache['expire'] > time()) {
      return $aCache['data'];
    }

    $path = $this->getModulePath($module_name);
    $profile = $_REQUEST['install-profile'] ?? \Drupal::installProfile();
    $module_dir = dirname(__DIR__, 2);
    $cmd = __DIR__ . "/../../bin/screenshottest \
      --install-profile=$profile \
      --status $path 2>&1";

    $json = `$cmd`;
    $modules = json_decode($json);
    if ($modules === NULL) {
      $this->handleInvalidJson($module_dir, $json);
      return [];
    }

    // Only store if we loaded all differences
    if (!$module_name) {
      \Drupal::state()->set('screenshots', [
        'expire' => time() + 20,
        'data' => $modules,
      ]);
    }

    return $modules;
  }

  public function title($module_name) {
    return "Module $module_name";
  }

  private function checkForRecommendedDevelopmentSetting(&$form): bool {
    if (ini_get('zend.assertions') != 1) {
      $form ['error']['#markup'] = sprintf(
          'You must enable %s in %s.',
          '"zend.assertions"',
          php_ini_loaded_file()) . '<br><br>' .
        'Please also check <a href="https://api.drupal.org/api/drupal/sites%21example.settings.local.php/8.6.x">these recommendations for your local settings.php</a>.';
      return FALSE;
    }

    // All is fine.
    return TRUE;
  }

  public function viewTable($module_name = NULL) {
    $form = [
      '#cache' => ['max-age' => 0],
      '#attached' => ['library' => ['qd_screenshottests/view_table']],
    ];
    if (!$this->checkForRecommendedDevelopmentSetting($form)) {
      return $form;
    }

    $all = \Drupal::request()->query->get('all');
    $skip_missing = \Drupal::request()->query->get('skip_missing');

    $form['display'] = [
      '#type' => 'item',
      '#title' => 'Display:',
      '#markup' => $all ?
        '<a href="?all=0"> Skip equal screenshots</a>' :
        '<a href="?all=1"> View all screenshots</a>',
    ];

    if (!$all) {
      $form['display']['#markup'] .=
        ' | ' . ($skip_missing ?
          '<a href="?skip_missing=0"> Show missing screenshots</a>' :
          '<a href="?skip_missing=1"> Skip missing screenshots</a>');
    }

    if ($module_name)
      $modules = [$this->getModuleByName($module_name)];
    else
      $modules = $this->modules();

    $rows = [];
    $pathname = Url::fromRoute('qd_screenshottests.view_table')->toString();

    $i = 1;
    $iSkippedEquals = 0;
    foreach ($modules as $module) {
      $screenshot_rows = [];
      $module_path = realpath($module->path);
      $optogram_path = $module_path . '/new/optogram.png';
      if (file_exists($optogram_path)) {
        $screenshot_rows[] = [
          'class' => 'screenshot-modified',
          'data' => [
            'name' => 'optogram.png',
            'status' => ['data' => 'error',],
            'fix' => '',
            'view' => [
              'class' => 'view',
              'data' => $this->get_screenshot_link('view', $module, 'optogram.png'),
            ],
          ],
        ];
      }

      foreach ($module->screenshots as $s => $screenshot) {
        if ($screenshot->status === 'equal' && !$all) {
          $iSkippedEquals++;
          continue;
        }
        if ($screenshot->status === 'missing' && $skip_missing)
          continue;

        $row = get_object_vars($screenshot);
        $row['status'] = [
          'data' => $screenshot->status,
          'title' => $this->getStatusDescription($screenshot),
        ];

        $titles = [
          'modified' => 'accept new screenshot',
          'missing' => 'remove old screenshot',
          'unlisted' => 'register new screenshot',
        ];

        $id = 'screenshot_id_' . $i++;
        $row['fix'] = $screenshot->status !== 'equal' ? [
          'data' => [
            '#type' => 'checkbox',
            '#title' => Markup::create("<label for='$id' class='option'>{$titles[$screenshot->status]}</label>"),
            '#attributes' => [
              'title' => 'Use shift+click for range-selection.',
              'id' => [$id],
            ],
          ],
          'class' => 'fix',
        ] : '';

        $text = $screenshot->status === 'modified' ? 'compare' : 'view';
        $link = $this->get_screenshot_link($text, $module, $screenshot->name);

        $row['view'] = ['data' => $link, 'class' => 'view'];

        $fix = '';
        if ($screenshot->status === 'missing') {
          $fix .= "rm $module_path/old/{$screenshot->name} \n";
          $fix .= "rmdir $module_path/old --ignore-fail-on-non-empty";
        }
        if ($screenshot->status === 'unlisted') {
          $fix .= "mkdir -p $module_path/old\n";
          $fix .= "curl $pathname/module/$module->module/screenshot/$screenshot->name/new \\\n";
          $fix .= "  -o $module_path/old/{$screenshot->name}";
        }
        if ($screenshot->status === 'modified') {
          $fix .= "curl $pathname/module/$module->module/screenshot/$screenshot->name/new \\\n";
          $fix .= "  -o $module_path/old/{$screenshot->name}";
        }
        $screenshot_rows[] = [
          'class' => 'screenshot-' . $screenshot->status,
          'data' => $row,
          'data-fix' => $fix,
        ];
      }
      if (!count($screenshot_rows)) {
        continue;
      }

      $td = ['colspan' => 99];
      $td['data'] = [
        '#type' => '#markup',
        '#markup' => "
            <a title='Show only screenshots from this module.'
              href='$pathname/module/{$module->module}?all=$all'>{$module->module}<a>",
      ];
      $rows[] = ['module' => $td];
      $rows = array_merge($rows, $screenshot_rows);
    }

    $form['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#empty' => $iSkippedEquals ?
        t('There are :equals equal screenshots', [':equals' => $iSkippedEquals]) :
        t('No screenshots have been created yet.'),
      '#attributes' => ['id' => 'screenshots'],
    ];
    if (!$rows) {
      return $form;
    }

    $form[] = ['#markup' => '<br>'];

    $form['copy'] = [
      '#markup' => "<a id='copy' class='fa fa-copy'></a>",
    ];

    $form['fixes'] = [
      '#type' => 'html_tag',
      '#tag' => 'pre',
      '#value' => '# Check any of the boxes above '
        . 'to fill this with shell commands.',
      '#attributes' => ['id' => 'fixes'],
    ];

    return $form;
  }

  private function get_screenshot_link($text, $module, $screenshot_name) {
    return Link::createFromRoute($text,
      'qd_screenshottests.view_screenshot', [
        'module_name' => $module->module,
        'screenshot_name' => $screenshot_name,
      ]);
  }

  public function viewScreenshot($module_name, $screenshot_name) {
    $form = ['#cache' => ['max-age' => 0]];

    $module = $this->getModuleByName($module_name);
    if ($screenshot_name === 'optogram.png') {
      $screenshot = new \stdClass();
      $screenshot->name = 'optogram.png';
      $screenshot->status = 'optogram';
    }
    else {
      $screenshot = $this->getArrayElementWithKeyValue($module->screenshots, $screenshot_name, 'name');
    }

    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'label',
      '#value' => "Screenshot: {$screenshot->name}",
    ];

    $form[] = ['#markup' => '<br>'];

    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'label',
      '#value' => "Module: {$module->module}",
    ];

    $form[] = ['#markup' => '<br>'];

    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'label',
      '#value' => "Status: " . $this->getStatusDescription($screenshot),
    ];

    if ($screenshot->status === 'modified') {
      $form[] = [
        '#markup' => 'Click on the image to toggle between '
          . '<u id="old">old</u> and <u id="new">new</u> version and '
          . 'the <u id="diff">diff</u>.',
      ];
    }

    $src = "new";
    if ($screenshot->status === 'missing') {
      $src = "old";
    }
    if ($screenshot->status === 'modified') {
      $src = "diff";
    }

    $form[] = ['#markup' => '<br>'];

    $form[] = [
      '#type' => 'html_tag',
      '#tag' => 'image',
      '#attributes' => ['id' => 'screenshot', 'src' => $src],
    ];

    $form['buttons'] = ['#type' => 'actions'];

    $form['#attached'] = ['library' => ['qd_screenshottests/view_screenshot']];

    if ($screenshot->status === 'modified') {
      $form['#attached']['library'][] = 'qd_screenshottests/toggle_screenshots';
      $oReplaceNewByOld = AccessAwareButton::createFromRoute(
        'C: ' . t('Discard actual state'),
        'qd_screenshottests.replace_new_by_old',
        [
          'module_name' => $module->module,
          'screenshot_name' => $screenshot_name,
        ]);
      $form['buttons'][] = $oReplaceNewByOld->toRenderable();
    }

    return $form;
  }

  private function getStatusDescription(\stdClass $screenshot) {
    $descriptions = [
      'modified' =>
        "The test generated a different screenshot " .
        "than in the repository.",
      'missing' => "The test failed to generate the screenshot.",
      'unlisted' =>
        "The screenshot was generated unexpectedly. " .
        "It is not in the 'old' directory of the repository yet.",
      'equal' => "Screenshots are equal.",
      'optogram' => "The test execution stopped prematurely.",
    ];
    return $descriptions[$screenshot->status];
  }

  /**
   * Returns an image.
   *
   * @param string $module_name
   *   The machine name of the module, for example "qd_screenshottests"
   * @param string $screenshot_name
   *   The file name without path. See qd_screenshottests/modules/capture.js:53.
   *   The format is "$install_profile.$js_test.$capture_name.png".
   * @param string $folder
   *   One of the following: "old", "new" or "diff".
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function getPng($module_name, $screenshot_name, $folder) {
    $module_path = realpath($this->getModulePath($module_name));

    $path = "$module_path/tests/$folder/$screenshot_name";
    if (!file_exists($path))
      throw new NotFoundHttpException();

    return new Response(file_get_contents($path), 200, ['content-type' => 'image/png']);
  }

  private function getArrayElementWithKeyValue($array, $value, $key) {
    foreach ($array as $element) {
      if ($element->$key == $value)
        return $element;
    }
    throw new \RuntimeException("'$value' was not found in " . json_encode($array));
  }

  /**
   * @param string $module_dir
   *   This module's installation directory.
   * @param string $invalid_json
   *   The output from the Bash script. It could not be parsed as valid JSON.
   */
  protected function handleInvalidJson(string $module_dir, $invalid_json): void {
    $sMkDirError = "mkdir: cannot create directory 'diff': Permission denied";
    if (strpos($invalid_json, $sMkDirError) > 0) {
      $aCommand = [];
      $aLines = explode("\n", $invalid_json);
      foreach (array_keys($aLines, $sMkDirError, TRUE) as $iErrorPos) {
        for ($iLine = $iErrorPos; $iLine > 0; $iLine--) {
          if (preg_match('/"path": *"([^"]+)"/', $aLines[$iLine], $aMatches)) {
            $sPath = $aMatches[1];
            $aCommand[] = "mkdir -p $sPath && chmod 0777 $sPath\n";
            break;
          }
        }
      }
      if ($aCommand) {
        \Drupal::messenger()
          ->addError(t('Please run the following shell commands to fix permission problems:'));
        $sPwd = DRUPAL_ROOT;
        $sCommands = join('', array_unique($aCommand));
        \Drupal::messenger()
          ->addError(Markup::create("<pre>cd $sPwd\n$sCommands</pre>"));
        return;
      }
    }

    \Drupal::messenger()
      ->addError(
        t(
          'CLI bash script "screenshottest" did not return valid JSON. ' .
          'Please run "<tt>cd :dir && make && sudo make install</tt>" ' .
          'to prepare your system.', [
          ':dir' => $module_dir,
        ]))
      ->addError(Markup::create(t('Output') . ': ' . "<pre>$invalid_json</pre>"));
  }
}
