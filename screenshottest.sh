#!/usr/bin/env bash

# Make will replace the following line with the actual path:
export MODULE_PATH=/var/www/html/web/modules/contrib/qd_screenshottests

CASPERJS="$MODULE_PATH/casperjs-1.1.*/bin/casperjs test --fail-fast"

if test -z "$DRUSH"; then
  export DRUSH=/var/www/html/vendor/bin/drush
  if test ! -f $DRUSH; then
    # The $DRUSH variable doesn't seem to point to a file.
    # Use the path found by 'make'.
    export DRUSH=$MODULE_PATH/../../../../vendor/bin/drush
  fi
  if test ! -f $DRUSH; then
    # The $DRUSH variable doesn't seem to point to a file. Let's try just 'drush'.
    export DRUSH=drush
  fi
fi

declare -a valid_profiles=($(find $MODULE_PATH/../../../profiles | grep .info.yml | \
  xargs grep 'type: profile' --files-with-matches -- | \
  sed 's/.*\///' | sed 's/.info.yml//'))

while true; do
  case ${1} in
  --help)
    cat << EOF
Usage: screenshottest [OPTION]... [SCRIPT|TEST_PATH]
Find and execute screenshot test scripts in the given TEST_PATH
or execute a single SCRIPT. If neither are given, the current directory is used.
Then, compare results with reference screenshots and generate image diffs.
Only .js files directly within a _tests_ directory are considered test scripts
for this Drupal module.

Database operations:
  --recreate-test-db  Recreate the test database (a snapshot of the database
                      after a drush site:install) that is loaded before each
                      script.
                      Aliases: --recreate-database | --recreate-testdb
  --install-profile=INSTALL_PROFILE
                      Use INSTALL_PROFILE when recreating the test database.
                      Valid profiles are:
                      ${valid_profiles[@]}
  --skip-recovery     Don't restore the database afterwards. This is useful if
                      tests fail.
  --drop-recovery     Delete the previous recovery dump and don't restore
                      the database afterwards. This implies --skip-recovery and
                      is useful to speed up test development if your previous
                      database was huge. Unless you specify --drop-recovery, the
                      screenshottest command will create a recovery dump before
                      anything else.

Test execution:
  --batch=CURRENT_RUN/TOTAL_RUNS
                      Split the modules into TOTAL_RUNS parts. This will
                      calculate a crc32 of each Drupal module path and only run
                      the tests in that module if
                         crc32 mod TOTAL_RUNS == CURRENT_RUN mod TOTAL_RUNS.
                      This can be useful to speed up execution by running tests
                      in parallel on separate VMs or Docker containers. For
                      example, to split the tests in half, run the following in
                      parallel:
                         screenshot --batch=1/2
                         screenshot --batch=2/2
  --base-url=BASE_URL Run tests on BASE_URL. This replaces the strings
                      http://192.168.33.22 and http://localhost in all CasperJS
                      requests.
  --stop-on-error     Abort as soon as the first error is encountered.
                      Alias: --abort-on-error
  --record-coverage   Record code coverage of the tests. Will automatically
                      recreate the test database.
  --number-of-tries=2 Re-try the tests in each module up to N times if they fail.
                      Default is 1 which means no retries.
  --compare           Don't run any tests and update image diffs instead.
  --status            Don't run any tests and print a JSON summary instead.
  --verbose           Show much more output.

Miscellaneous:
  --email=EMAIL       Send diff images to EMAIL, if any differences are
                      encountered. For a GitLab CI pipeline, this can be
                      \$GITLAB_USER_EMAIL.
  --help              Display this help text and exit.
EOF
    exit
    ;;
  --recreate-database | \
  --recreate-test-db | \
  --recreate-testdb)
    RECREATE_TEST_DB=1
    ;;
  --base-url=*)
    CASPERJS="$CASPERJS ${1}"
    export QD_BASE_URL=${1}
    ;;
  --batch=*)
    CURRENT_RUN=$(echo ${1} | sed -nr 's/--batch=([0-9]+)\/[0-9]+/\1/p')
    TOTAL_RUNS=$(echo ${1} | sed -nr 's/--batch=[0-9]+\/([0-9]+)/\1/p')
    ;;
  --install-profile)
    # Fallthrough to install-profile=
    ;&
  --install-profile=*)
    CASPERJS="$CASPERJS ${1}"
    PROFILE=$(echo ${1} | sed -nr 's/--install-profile=(.+)/\1/p')
    if [[ ! " ${valid_profiles[@]} " =~ " $PROFILE " ]]; then
      echo "Invalid profile name '$PROFILE'. Here are valid install profiles:"
      for element in "${valid_profiles[@]}"; do
        echo "  --install-profile=$element"
      done
      exit 1
    fi
    ;;
  --email=*)
    EMAIL=$(echo ${1} | sed 's/--email=//;')
    ;;
  --stop-on-error | \
  --abort-on-error)
    ABORT_ON_ERROR=1
    ;;
  --skip-recovery)
    SKIP_RECOVERY=1
    ;;
  --drop-recovery)
    mkdir -p $MODULE_PATH/sql
    > $MODULE_PATH/sql/recovery.sql
    DROP_RECOVERY=1
    SKIP_RECOVERY=1
    ;;
  --record-coverage)
    RECORD_COVERAGE=1
    ;;
  --number-of-tries=*)
    NUMBER_OF_TRIES=$(echo ${1} | sed 's/--number-of-tries=//;')
    ;;
  --compare)
    COMPARE=1
    ;;
  --verbose)
    CASPERJS="$CASPERJS ${1}"
    ;;
  --status)
    STATUS=1
    ;;
  -*)
    echo "$(basename ${0}): unrecognized option '${1}'" 1>&2
    exit 2
    ;;
  *)
    # Obtain [SCRIPT|TEST_PATH] from first unmatched flag; it may be
    # omitted in which case TEST_PATH defaults to the current directory.
    test -n "$TEST_PATHS" && break

    if test -z "${1}" -o -d "${1}"; then
      TEST_PATHS=$(find -L ${1} -type d | grep '.*/tests$' | grep -v 'core/modules/')
    elif test -f ${1}; then
      # It is a file.
      TEST_PATHS=$(dirname ${1})
      SCRIPT=$(basename ${1})
    else
      echo "$(basename ${0}): ${1}: No such file or directory" 1>&2
      exit 3
    fi

    test "$TEST_PATHS" || TEST_PATHS="./ tests"

    echo ${1} | grep -q '^/' || PWD0=$PWD
    ;;
  esac
  shift
done

if test $STATUS; then
  # JSON output; ECHO does nothing
  ECHO=true
  PRINTF=printf
  NUMBER_OF_TRIES=1 # No need for retries when we just want status
else
  # Regular output; PRINTF does nothing
  ECHO=echo
  PRINTF=true
  DEFAULT_COLOR='\e[0m'
  DIFFERENT_COLOR='\e[37;41m'
  UNLISTED_COLOR='\e[37;1;43m'
  EQUAL_COLOR='\e[37;1;42m'
  MODULE_COLOR='\e[35;1;40m'
fi

function defer() {
  DEFERRED_COMMANDS="${*}; $DEFERRED_COMMANDS"
  trap "$DEFERRED_COMMANDS" EXIT
}

export QT_QPA_PLATFORM=phantom
PHANTOMJS=$MODULE_PATH/phantomjs-2.*-linux-x86_64

export PATH=$(echo $PHANTOMJS/bin):$PATH

# Directory where to create the dumps of both recovery copy of the current
# database and the test database. The directory is managed by this Bash script.
mkdir -p $MODULE_PATH/sql

# Handle creation and removal of database dumps:
if test -z "$STATUS" -a -z "$COMPARE"; then
  if test ! -f $MODULE_PATH/sql/recovery.sql; then
    $DRUSH sql-dump > $MODULE_PATH/sql/recovery.sql || exit 1
  fi

  if test -z $SKIP_RECOVERY; then
    defer rm $MODULE_PATH/sql/recovery.sql
    defer $DRUSH sql:cli \< $MODULE_PATH/sql/recovery.sql
  fi

  if test $DROP_RECOVERY; then
    defer rm $MODULE_PATH/sql/recovery.sql
  fi

  if test $RECREATE_TEST_DB; then
    rm -f $MODULE_PATH/sql/$PROFILE.sql
  fi

  if test ! -f $MODULE_PATH/sql/$PROFILE.sql; then
    $DRUSH site:install $PROFILE -y \
      --account-mail=admin@quodata.de \
      --account-pass=admin || exit 1
    $DRUSH cache:rebuild
    $DRUSH php:script $MODULE_PATH/install_demo_content.php || exit 1
    $DRUSH pm:enable qd_screenshottests

    # Make sure all tests start with an empty list at /admin/reports/dblog
    $DRUSH sql:query "DELETE FROM watchdog"

    $DRUSH sql-dump > $MODULE_PATH/sql/$PROFILE.sql
  fi
fi

if test -z $PROFILE; then
  PROFILE=$($DRUSH php:eval 'use Drupal\Core\Site\Settings;
                            print(\Drupal::installProfile());')
  if test $? != 0; then
    echo $(basename ${0}): could not determine \
      install profile and none given. Please \
      use the --install-profile=FOO parameter 1>&2
    exit 1
  fi
  CASPERJS="$CASPERJS --install-profile=$PROFILE"
fi

FIFO=$(mktemp -u)
mkfifo $FIFO
defer rm $FIFO

# Generate a list of all testable Drupal modules:
for TEST_PATH in $TEST_PATHS; do
  cd $PWD0/$TEST_PATH 2> /dev/null || continue
  test -f ../*info.yml || continue
  ls *.js >& /dev/null || continue

  if [[ $TOTAL_RUNS ]]; then
    MODULE_CRC=$(php -r "echo abs(crc32('$TEST_PATH'));")
    MODULE_BELONGS_TO_RUN=$((MODULE_CRC % TOTAL_RUNS + 1))
    if [[ $MODULE_BELONGS_TO_RUN != $CURRENT_RUN ]]; then
      # Skip, not part of current run.
      continue
    fi
  fi

  # Yep, this is a testable Drupal module - write it into our pipe:
  echo $TEST_PATH
done > $FIFO &

$PRINTF '['

run_tests() {
  for _SCRIPT in $SCRIPTS; do
    if [[ $_SCRIPT == "*."* ]]; then
      # Didn't find a script of that type, so either no *.js or no *.selenium.php
      continue;
    fi

    if grep "'modules/exert-profiles'" $_SCRIPT | grep -qv $PROFILE; then
      # This test is for a different profile and will therefore be skipped.
      continue
    fi

    UPDATE_DB=0
    SQL_FILE=""
    if grep -q "'modules/load-sql'" $_SCRIPT; then
      # Use specific database.
      # Greps the absolute path until ".sql" or ".sql.gz" and only the first result
      SQL_FILE=$(grep -om1 "[^'\"]*\.sql[\.gz]*" $_SCRIPT)
    fi
    if [[ $SQL_FILE == *".sql"* ]]; then
      UPDATE_DB=1
    else
      # Default database
      SQL_FILE=$MODULE_PATH/sql/$PROFILE.sql
    fi

    # Reset to test database (fixture)
    $DRUSH sql:drop --quiet
    # Unzip database if compressed
    if [[ $SQL_FILE == *".gz"* ]]; then
        echo ".gz file found, unpacking..."
        gzip -dc "$SQL_FILE" | $DRUSH sql:cli
    else
      $DRUSH sql:cli < "$SQL_FILE"
    fi
    $DRUSH cache:rebuild
    # Update database if one was specified.
    if [[ $UPDATE_DB == 1 ]]; then
      echo "Updating database..."
      $DRUSH updb -y --quiet
      $DRUSH cache:rebuild
      $DRUSH pm:enable qd_screenshottests
    fi
    $DRUSH php:eval "\Drupal::state()->set('current_screenshottest', '$MODULE/tests/$_SCRIPT');"
    if test $RECORD_COVERAGE; then
      $DRUSH en qd_screenshottests_route_coverage_recorder
    fi

    if [[ $_SCRIPT == *.selenium.php ]]; then
      $DRUSH php:script $_SCRIPT || RESULT=1
      pkill geckodriver || true
      pkill chromedriver || true
      pkill firefox || true
    else
      $CASPERJS $_SCRIPT || RESULT=1
    fi


    if test -f new/optogram.png; then
      # CasperJS failed to complete the script and created a picture of the
      # browser - let's send it to the person who triggered the CI pipeline.
      ATTACHMENTS="$ATTACHMENTS -A $PWD0/$TEST_PATH/new/optogram.png"
      ATTACHMENTS="$ATTACHMENTS -A $PWD0/$TEST_PATH/new/optogram-DOM.html"
    fi
    source $MODULE_PATH/show_errors_from_watchdog.sh
    # Do not run other scripts on failure when ABORT_ON_ERROR is set
    test "$RESULT" -a "$ABORT_ON_ERROR" && break
  done
}

compare_pngs() {
  PATTERN='.*\.png$'
  if test $SCRIPT; then
    SCRIPT_BASENAME=$(basename -s .js -s .selenium.php $SCRIPT)
    PATTERN=$SCRIPT_BASENAME'\.'$PATTERN
  fi
  PATTERN='^'$PROFILE'\.'$PATTERN
  PNGS=$({
    ls old
    ls new
  } 2> /dev/null | grep $PATTERN | sort -u)
  for PNG in $PNGS; do
    $PRINTF '%s\n' $COMMA && COMMA=','
    $PRINTF '      {                 \n'
    $PRINTF '        "name": "%s",   \n' $PNG

    if test ! -f new/$PNG; then
      $ECHO -e "$MISSING_COLOR* $PNG: screenshot required but missing$DEFAULT_COLOR"
      $PRINTF '        "status": "missing"\n'
      $PRINTF '      }'
      RESULT=1
      continue
    fi

    if test ! -f old/$PNG; then
      $ECHO -e "$UNLISTED_COLOR* $PNG: screenshot is not registered yet$DEFAULT_COLOR"
      $PRINTF '        "status": "unlisted"\n'
      $PRINTF '      }'
      RESULT=1
      ATTACHMENTS="$ATTACHMENTS -A $PWD0/$TEST_PATH/new/$PNG"
      continue
    fi

    if cmp -s old/$PNG new/$PNG; then
      $ECHO -e "$EQUAL_COLOR* $PNG: screenshots are equal$DEFAULT_COLOR"
      $PRINTF '        "status": "equal" \n'
      $PRINTF '      }'
      continue
    fi

    # See https://imagemagick.org/script/compare.php
    mkdir -p diff
    FUZZ='-fuzz 0% -metric AE'
    compare $FUZZ old/$PNG new/$PNG diff/$PNG 2> /dev/null

    # The compare program returns 2 on error, 0 if the images are similar,
    # or a value of 1 if they are not similar.
    COMPARE_RESULT=$?

    if test $COMPARE_RESULT == 0; then
      # While the files are binary different, the color difference is less than
      # FUZZ.
      $ECHO -e "$EQUAL_COLOR* $PNG: screenshots are equal$DEFAULT_COLOR"
      $PRINTF '        "status": "equal" \n'
      $PRINTF '      }'
      continue
    fi

    $ECHO -e "$DIFFERENT_COLOR* $PNG: screenshots are different$DEFAULT_COLOR"
    $PRINTF '        "status": "modified"\n'
    $PRINTF '      }'
    RESULT=1

    # These are the name patterns used in emails:
    # - old.profile.test_file.capture_name.png
    # - profile.test_file.capture_name.png  <--- if this is alright: commit it
    # - diff.profile.test_file.capture_name.png
    PNG_TEMP_DIR=$(mktemp -d -t)
    OLD_PNG_FOR_EMAIL=$PNG_TEMP_DIR/old.$PNG
    cp $PWD0/$TEST_PATH/old/$PNG $OLD_PNG_FOR_EMAIL
    ATTACHMENTS="$ATTACHMENTS -A $OLD_PNG_FOR_EMAIL"

    ATTACHMENTS="$ATTACHMENTS -A $PWD0/$TEST_PATH/new/$PNG"
    if test -f "$PWD0/$TEST_PATH/new/$PNG-DOM.html"; then
      ATTACHMENTS="$ATTACHMENTS -A $PWD0/$TEST_PATH/new/$PNG-DOM.html"
    fi

    # The diff.png might not be created at this point, we need to copy it again at the end
    DIFF_PNG_FOR_EMAIL=$PNG_TEMP_DIR/diff.$PNG
    if test -f $PWD0/$TEST_PATH/diff/$PNG; then
      cp $PWD0/$TEST_PATH/diff/$PNG $DIFF_PNG_FOR_EMAIL
    fi
    ATTACHMENTS="$ATTACHMENTS -A $DIFF_PNG_FOR_EMAIL"

    test "$STATUS" -a -z "$COMPARE" && continue

    # Go to next picture if dimensions were equal:
    test $COMPARE_RESULT == 2 || continue

    W=$(identify -format "%W\n" old/$PNG new/$PNG | sort -n | head -n 1)
    H=$(identify -format "%H\n" old/$PNG new/$PNG | sort -n | head -n 1)
    CROP=${W}x${H}+0+0

    # Find the way to crop that produces the smallest absolute error (AE)
    # count, i.e. the number of different pixels; see
    # https://imagemagick.org/script/command-line-options.php#metric
    MIN_AE='-log(0)'
    for GRAVITY in NorthWest NorthEast SouthEast SouthWest; do
      convert -gravity $GRAVITY -crop $CROP old/$PNG old/$PNG.cropped
      convert -gravity $GRAVITY -crop $CROP new/$PNG new/$PNG.cropped
      AE=$(compare $FUZZ \
        old/$PNG.cropped new/$PNG.cropped \
        diff/$PNG.cropped >&/dev/stdout)

      if awk "BEGIN{exit $AE >= $MIN_AE}"; then
        mv diff/$PNG.cropped diff/$PNG
        MIN_AE=$AE
      fi

      rm -f {old,new,diff}/$PNG.cropped
    done
    cp $PWD0/$TEST_PATH/diff/$PNG $DIFF_PNG_FOR_EMAIL
  done
}

CURRENT_MODULE=0
while read TEST_PATH; do
  $PRINTF '%s\n' $(test $MODULE && echo ,)
  cd $PWD0/$TEST_PATH/../
  MODULE=$(basename $PWD)
  let CURRENT_MODULE+=1

  if test -z $COMPARE; then
    $ECHO -e "$MODULE_COLOR--- Module $CURRENT_MODULE - $MODULE: Running test scripts ---$DEFAULT_COLOR"
  fi

  cd $PWD0/$TEST_PATH

  if test $SCRIPT; then
    SCRIPTS=$SCRIPT
  else
    SCRIPTS=*.js
    PHPSCRIPTS=*.selenium.php
    SCRIPTS="$SCRIPTS $PHPSCRIPTS"
  fi

  test "$STATUS" -o "$COMPARE" && unset SCRIPTS

  # Try to rerun module tests if screenshots were different or CasperJS failed
  for i in $(seq 1 $NUMBER_OF_TRIES); do
    # Each module gets a clean start
    unset RESULT
    unset ATTACHMENTS
    test "$STATUS" -o "$COMPARE" || rm -rf new diff
    test "$COMPARE" && rm -rf diff

    run_tests

    test "$RESULT" && continue

    $ECHO -e "$MODULE_COLOR--- Module $CURRENT_MODULE - $MODULE: Screenshot test results ---$DEFAULT_COLOR"
    $PRINTF '  {                 \n'
    $PRINTF '    "module": "%s", \n' $MODULE
    $PRINTF '    "path": "%s",   \n' $TEST_PATH
    $PRINTF '    "screenshots": ['

    unset COMMA

    compare_pngs

    $ECHO
    $PRINTF '\n    ]\n'
    $PRINTF '  }'

    test "$RESULT" && continue

    break # No need to rerun if RESULT is not set
  done

  # Stop whole test on failure when ABORT_ON_ERROR is set
  test "$RESULT" -a "$ABORT_ON_ERROR" && break

done < $FIFO

$PRINTF '\n]\n'

if test $EMAIL -a "$ATTACHMENTS"; then
  # Schlechte Bilder per Mail schicken. Für die Parameter siehe
  # https://mailutils.org/manual/mailutils.html#Invoking-Mail
  for VAR in MODULE TEST_PATH CI_COMMIT_REF_NAME CI_COMMIT_MESSAGE \
             CI_COMMIT_SHA  CI_RUNNER_DESCRIPTION CI_JOB_URL; do
    echo $VAR
    eval echo \$$VAR
    echo
  done | mail -s "$CI_PROJECT_NAME | $CI_COMMIT_REF_NAME | Screenshot test results [$PROFILE]" $ATTACHMENTS $EMAIL \
              -a "Content-Type: text/plain; charset=UTF-8" \
              -a "From: screenshottest@quodata.de"
  if test $? -gt 0; then
    echo ERROR: Could not send email. Please ensure that \
      $(mail --version) says GNU Mailutils. 1>&2
    printenv
  fi

  # Die GitLab-CI zerstört den Docker-Container direkt nach dem Ende dieses
  # Skriptes. Docker Zeit geben, damit die Mail auch an Exchange übergeben wird:
  sleep 90
fi

# If you did not use ABORT_ON_ERROR this might return 0 even if there were failures
exit $RESULT
