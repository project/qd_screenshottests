#!/usr/bin/env bash
if [[ -z "$DRUSH" ]]; then
  echo 'The variable $DRUSH needs to be set.'
  exit 1
fi

$DRUSH sql:query "DELETE FROM watchdog WHERE variables LIKE '%Extension has no unserializer%';"

QUERY="SELECT wid FROM watchdog WHERE type='php' OR severity < 4;"
for WID in `$DRUSH sql:query "$QUERY" | tail -n+2`; do
  RESULT=1
  $DRUSH watchdog:show-one --format=table $WID
  $DRUSH sql-query "SELECT variables FROM watchdog WHERE wid = $WID"
done
