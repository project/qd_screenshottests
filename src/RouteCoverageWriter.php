<?php

namespace Drupal\qd_screenshottests;

use Consolidation\Comments\Comments;
use Drupal\Component\Discovery\YamlDiscovery;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Serialization\Yaml;

class RouteCoverageWriter extends YamlDiscovery {

  public const TEST_COVERAGE_DIR = 'public://screenshottest_coverage';

  public const ENTITY_ROUTES_DIR = self::TEST_COVERAGE_DIR . '/module_entity_routes/';

  public $aFilesToUpdate = [];

  public $aRoutesThatLostCoverage = [];

  private $sDirectory;

  public $bDryRun = TRUE;

  public $iTotalRoutes = 0, $iCoveredRoutes = 0;

  private $oCommentManager;

  /** @var string[] All routing files of currently installed modules. */
  private $aRoutingFiles;

  public function __construct($sDirectory, $bAddAllDirs) {
    $this->sDirectory = $sDirectory;
    $this->oCommentManager = new Comments();

    $aDirs = $bAddAllDirs ?
      $this->findAllModuleDirs() :
      \Drupal::moduleHandler()->getModuleDirectories();

    parent::__construct('routing', $aDirs);
    $this->aRoutingFiles = $this->findFiles();
  }

  private function findAllModuleDirs() {
    $oIterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($this->sDirectory));
    $aModuleDirs = [];
    foreach (new \RegexIterator($oIterator, '/routing.yml/', \RegexIterator::GET_MATCH) as $sFilePath => $sFile) {
      $sDir = dirname($sFilePath);
      $aModuleDirs[basename($sDir)] = $sDir;
    }
    return $aModuleDirs;
  }

  /**
   * @return string
   *   File path of first file to be modified that is not under Git tracking.
   *   - or -
   *   Empty string if all files to be modified are under source control.
   */
  public function checkForUntracked(): string {
    $this->bDryRun = TRUE;
    $this->updateRoutingFiles();
    foreach ($this->aFilesToUpdate as $sFile) {
      $sDir = dirname($sFile);
      $sCmd = "git -C '$sDir' ls-files --error-unmatch '$sFile'";
      system($sCmd, $iErrorLevel);
      if ($iErrorLevel) {
        return $sFile;
      }
    }
    return '';
  }

  /**
   * Adds YAML keys to Drupal routing files. This documents which routes have
   * test coverage.
   *
   * @return bool TRUE if there are potential routes to write.
   */
  public function updateRoutingFiles(): bool {
    // The coverage collected by running the `screenshottest` shell script.
    $aRouteCoverages = RouteCoverageReader::getRouteCoverage();
    if (!$aRouteCoverages)
      return FALSE;

    // The .routing.yml files from all modules.
    $aRouteDefs = $this->findAll();
    foreach ($aRouteDefs as $sModule => $aRoutes) {
      $sEntityRouteFile = self::ENTITY_ROUTES_DIR . $sModule;
      if (file_exists($sEntityRouteFile))
        $aRoutes += json_decode(file_get_contents($sEntityRouteFile), TRUE);

      $this->handleSingleRoutingFile($aRouteCoverages, $sModule, $aRoutes);
    }
    return TRUE;
  }

  /**
   * @param array $aRouteCoverages
   *   An array keyed by route that contains an array keyed by javascript test
   *   file that contains the install profile machine names.
   *
   *   For example:
   *   $aRouteCoverages = [
   *     "qd_lab_entity.revisions" => [
   *       "edit.js" => [
   *         "custom_profile_1",
   *         "custom_profile_2",
   *       ],
   *     ]
   *   ];
   * @param string $sModule
   *   The module name, e.g. "webform".
   * @param array $aRoutes
   *   The parsed content of the .routing.yml file.
   */
  private function handleSingleRoutingFile(array $aRouteCoverages, string $sModule, array $aRoutes) {
    $sFilename = $this->aRoutingFiles[$sModule];
    $sFilename = str_replace('.routing.yml', '.qd_screenshottests.yml', $sFilename);
    if (strpos($sFilename, $this->sDirectory) !== 0) {
      // This file is outside of our directory to mess in.
      return;
    }

    $bNeedsUpdate = FALSE;

    $this->iTotalRoutes += count($aRoutes);

    // Insert or update section:
    foreach (array_intersect_key($aRouteCoverages, $aRoutes) as $sRouteName => $aCoverage) {
      $this->updateRoute(
        $aRoutes[$sRouteName],
        $aRouteCoverages[$sRouteName],
        $bNeedsUpdate);
      $this->iCoveredRoutes++;
    }

    // Update section if there is no coverage:
    foreach (array_diff_key($aRoutes, $aRouteCoverages) as $sRouteName => $aRoute) {
      $bRouteWasSetBefore = $this->updateRoute(
        $aRoutes[$sRouteName],
        'NO-COVERAGE-YET!',
        $bNeedsUpdate);
      if ($bRouteWasSetBefore) {
        $this->aRoutesThatLostCoverage[] = $sRouteName;
      }
    }

    if (!$bNeedsUpdate)
      return;

    $this->aFilesToUpdate[] = $sFilename;
    $sFileContent = file_exists($sFilename) ? file_get_contents($sFilename) : '';
    $aLines = explode("\n", $sFileContent);
    $this->oCommentManager->collect($aLines);
    $sAlteredFileContent = Yaml::encode($aRoutes);

    // Inject comments back into altered result:
    $aLines = $this->oCommentManager->inject(explode("\n", $sAlteredFileContent));

    if (!$this->bDryRun) {
      file_put_contents(
        $sFilename,
        join("\n", $aLines) . "\n");
    }
  }

  /**
   * @param $aRoute
   *   The Drupal route.
   * @param $aRouteCoverage
   *   The coverage recorded from previous `screenshottest` commands.
   * @param $bNeedsUpdate
   *   YAML file needs to be written.
   *
   * @return bool
   *   TRUE if coverage has changed. FALSE if no change or no key existed
   *   before.
   */
  private function updateRoute(&$aRoute, $aRouteCoverage, &$bNeedsUpdate) {
    $bIsSet = !empty($aRoute);
    if ($bIsSet && $aRoute == $aRouteCoverage)
      return FALSE;

    $aRoute = $aRouteCoverage;
    $bNeedsUpdate = TRUE;
    return $bIsSet;
  }

  public function getCoverageText() {
    return sprintf(
      " Summary:\n" .
      "   Routes: %.2f %% (%d/%d)\n",
      $this->iCoveredRoutes / $this->iTotalRoutes * 100,
      $this->iCoveredRoutes,
      $this->iTotalRoutes);
  }

  public static function deleteCoverageDir() {
    /** @var \Drupal\Core\File\FileSystem $oFileSystem */
    $oFileSystem = \Drupal::service('file_system');
    $oFileSystem->deleteRecursive(self::TEST_COVERAGE_DIR);
  }

  public static function markAsUsed($sRoute, $bCapture = FALSE) {
    $sCurrentTest = \Drupal::state()->get('current_screenshottest');
    /** @var \Drupal\Core\File\FileSystem $oFileSystem */
    $oFileSystem = \Drupal::service('file_system');
    $sDir = self::TEST_COVERAGE_DIR . "/$sRoute/$sCurrentTest";
    if (!is_dir($sDir))
      $oFileSystem->mkdir($sDir, 0777, TRUE);
    $sProfile = \Drupal::installProfile();
    $sFile = $sDir . "/profiles";

    $sContent = file_exists($sFile) ? file_get_contents($sFile) : '{}';
    $aProfiles = json_decode($sContent, TRUE) ?: [];
    if (empty($aProfiles[$sProfile]))
      $aProfiles[$sProfile] = $bCapture;
    file_put_contents($sFile, json_encode($aProfiles));
    chmod($sFile, 0777);
  }

  /**
   * Get all currently active entity routes (that are normally not present in
   * routing.yml files) and write them as available routes to the coverage
   * directory, so that they can be considered when calling
   * "drush qd_screenshottests:coverage-to-routing".
   *
   * The created files have the module name and as file content their respective
   * entity routes encoded as a json dictionary, e.g.:
   *  {'entity.node.edit_form': 1, 'entity.node.collection': 1}
   */
  public static function writeAvailableEntityRoutes() {
    \Drupal::state()
      ->set('qd_screenshottests_route_coverage_recorder.entity_routes_set', 1);
    if (!is_dir(self::ENTITY_ROUTES_DIR) && !mkdir(self::ENTITY_ROUTES_DIR, 0777, TRUE)) {
      throw new \RuntimeException('Could not create ' . self::ENTITY_ROUTES_DIR . ' folder.');
    }

    $aEntityTypeRoutes = [];

    /** @var \Drupal\Core\Routing\RouteProvider $router */
    $router = \Drupal::service('router.route_provider');
    foreach ($router->getAllRoutes() as $sRoute => $oRoute) {
      if (preg_match('/entity\.(.+?)\./', $sRoute, $aMatches))
        $aEntityTypeRoutes[$aMatches[1]][] = $sRoute;
    }

    foreach ($aEntityTypeRoutes as $sEntityType => $aRoutes) {
      try {
        $oDef = \Drupal::entityTypeManager()->getDefinition($sEntityType);
      } catch (PluginNotFoundException $e) {
        continue;
      }
      $sModuleFile = self::ENTITY_ROUTES_DIR . explode('\\', $oDef->getClass())[1];
      $aOldRoutes = is_file($sModuleFile) ? json_decode(file_get_contents($sModuleFile), TRUE) : [];
      $aNewRoutes = $aOldRoutes + array_flip($aRoutes);
      file_put_contents($sModuleFile, json_encode($aNewRoutes));
    }

  }

}
