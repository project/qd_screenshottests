module.exports = function (filter, offset) {
  casper.thenEvaluate(function (filter, offset) {
    var inputs = jQuery('input, textarea, select');
    inputs = inputs.filter(':enabled');
    inputs = inputs.filter(filter || '*');

    if (offset === undefined) {
      offset = 1;
    }

    // Text
    inputs.filter(':text').val(function (i) {
      return 'Text ' + (offset + i + 1);
    });

    // Memo
    inputs.filter('textarea').val(function (i) {
      return 'Textarea-Text ' + (offset + i + 1);
    });

    // Datum
    inputs.filter('input[type=date]').val(function (i) {
      var iDayBetween1And31 = (offset + i) % 31 + 1;
      return '2018-01-' + ('0' + iDayBetween1And31).substr(-2);
    });

    // Radio-Buttons
    inputs.filter(':radio').map(function () {
      if (this === jQuery('[name=' + this.name + ']')[0]) {
        return jQuery('[name=' + this.name + ']');
      }
    })
      .each(function (i, radios) {
        radios.eq((i + offset) % radios.length).attr('checked', true);
      });

    // Checkboxen
    inputs.filter(':checkbox').each(function (i) {
      i += offset;
      if (i % 2 === 0) {
        jQuery(this).attr('checked', true).change();
      }
    });

    // Dropdown-Menu
    inputs.filter('select').each(function (i) {
      var option = jQuery(this).find('option');
      jQuery(this).val(option.eq((offset + i + 1) % option.length).val())
        .change();
    });
  }, filter, offset);
};
