<?php

namespace Drupal\qd_screenshottests\Commands;

use Drupal\Core\Url;
use Drupal\qd_screenshottests\RouteCoverageWriter;
use Drush\Commands\DrushCommands;

/**
 * Drush commandfile to add test coverage information to routing files.
 */
class QdScreenshottestsCommands extends DrushCommands {

  /** @var RouteCoverageWriter */
  private $oRouteCoverageWriter;

  /**
   * @usage drush qd_screenshottests:coverage-to-routing
   *   Takes the screenshottest_coverage and adds the entries to special *.qd_screenshottests.yml files in your modules.
   *   This makes it transparent which routes need more coverage.
   *   It is only meant for humans.
   *
   * @param string $sDirectory
   *   Optional: the directory in which to add .qd_screenshottests.yml files. Defaults
   *   to current directory.
   * @param $options
   *
   * @option dry-run
   *   Change nothing.
   * @option all-modules
   *   Write to all modules not just the currently installed
   * @option force-against-untracked
   *   Allow this command to modify files that are not tracked by Git.
   *
   * @command qd_screenshottests:coverage-to-routing
   * @aliases c2r
   */
  public function coverageToRouting($sDirectory = '', array $options = [
    'force-against-untracked' => FALSE,
    'dry-run' => FALSE,
    'all-modules' => FALSE,
  ]) {
    /** @var \Drush\Log\Logger $oDrushLogger */
    $oDrushLogger = $this->logger();
    if (!$options['all-modules'])
      $oDrushLogger->info('Adding coverage for profile ' . \Drupal::installProfile());

    $sDirectory = $this->getAbsoluteDirectory($sDirectory);
    $this->oRouteCoverageWriter = new RouteCoverageWriter($sDirectory, $options['all-modules']);

    if (empty($options['force-against-untracked'])) {
      $sFilename = $this->oRouteCoverageWriter->checkForUntracked();
      if ($sFilename) {
        $oDrushLogger->error(dt(
          'The file "@file" is not tracked by Git. Please specify the --force-against-untracked option. ', ['@file' => $sFilename]));
        $oDrushLogger->error(dt('No file was modified.'));
        return;
      }
    }
    $this->oRouteCoverageWriter->bDryRun = !empty($options['dry-run']);

    // This is our main job:
    if (!$this->oRouteCoverageWriter->updateRoutingFiles()) {
      $oDrushLogger->error(dt(
        'There is no recorded coverage yet. Please run `screenshottest --record-coverage` first.'));
      return;
    }

    $iUpdateCount = count($this->oRouteCoverageWriter->aFilesToUpdate);
    if ($iUpdateCount) {
      $oDrushLogger->success(dt(
        !empty($options['dry-run']) ?
          'Would have updated !update_count .routing.yml files, but --dry-run is active.' :
          'Updated !update_count .routing.yml files.',
        ['!update_count' => $iUpdateCount]));
    }
    else {
      $oDrushLogger->success(dt('No files needed to be updated.'));
    }
    if ($this->oRouteCoverageWriter->aRoutesThatLostCoverage) {
      $sRoutes = join(', ', $this->oRouteCoverageWriter->aRoutesThatLostCoverage);
      $oDrushLogger->warning('The following routes lost screenshot test coverage: ' . $sRoutes);
    }
    if ($this->oRouteCoverageWriter->iTotalRoutes) {
      echo $this->oRouteCoverageWriter->getCoverageText();
    }
  }

  /**
   * @usage drush qd_screenshottests:delete-coverage-dir
   *   Deletes the screenshottest_coverage directory to remove all traces of
   *   previous test runs.
   * @command qd_screenshottests:delete-coverage-dir
   */
  public function deleteCoverageDir() {
    RouteCoverageWriter::deleteCoverageDir();
  }

  /**
   * @usage drush qd_screenshottests:record-capture
   *   Record that the last visited route was captured
   *
   * @param string $sUrl Current url while capturing
   *
   * @command qd_screenshottests:record-capture
   */
  public function recordCapture($sUrl) {
    if (!\Drupal::moduleHandler()
      ->moduleExists('qd_screenshottests_route_coverage_recorder'))
      return;

    $sUrl = preg_replace('/https?:\/\/.+?\//', '/', $sUrl);
    if (preg_match('/^\/(\w\w)\//', $sUrl, $aMatches)) {
      $sLangCode = $aMatches[1];
      if (isset(\Drupal::languageManager()->getLanguages()[$sLangCode]))
        $sUrl = substr($sUrl, 3);
    }

    try {
      $oUrl = Url::fromUserInput($sUrl);
      $sRoute = $oUrl->getRouteName();
    } catch (\Throwable $e) {
      return;
    }
    if ($sRoute)
      RouteCoverageWriter::markAsUsed($sRoute, TRUE);
  }

  private function getAbsoluteDirectory($sDirectory) {
    $sRootPath = getcwd();
    chdir($this->getConfig()->cwd());
    $sAbsoluteDirectory = realpath($sDirectory ?: '.');
    chdir($sRootPath);

    if ($sAbsoluteDirectory === FALSE) {
      throw new \InvalidArgumentException(dt(
        'The directory "@dir" is not valid.', ['@dir' => $sDirectory]));
    }
    return $sAbsoluteDirectory;
  }

}
