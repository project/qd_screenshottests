module.exports = function () {
  casper.thenEvaluate(function () {
    for (var name in CKEDITOR.instances) {
      CKEDITOR.instances[name].destroy();
    }
  });
};
