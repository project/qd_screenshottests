<?php

/**
 * @file
 * Hooks provided by the qd_screenshottests module.
 */

use Drupal\node\Entity\Node;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Fill database with demo content after a fresh site:install.
 *
 * This hook is invoked when a new Drupal installation for tests has finished.
 * A new installation happens when there is no file in
 * qd_screenshottests/sql/YOUR_PROFILE_NAME.sql or if the parameter --recreate-testdb
 * is used.
 */
function hook_demo_content_for_screenshottest() {
  Node::create([
    'type' => 'page',
    'title' => 'First demo news',
    'body' => 'Here goes the content of this news entry.',
  ])->save();
}

/**
 * @} End of "addtogroup hooks".
 */
