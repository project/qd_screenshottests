/**
 * This module returns a function to log into Drupal.
 *
 * Example for script with a anonymous Drupal session:
 *   require('modules/start');
 *
 * Example that logs in as admin:
 *   var login = require('modules/start');
 *   login('admin', 'admin');
 */
if (casper.cli.get('verbose')) {
  casper.options.verbose = true;
  casper.options.logLevel = 'debug';
}

casper.on('resource.error', function (err) {
  if (err.errorString == 'Operation canceled') {
    // Happens when AJAX requests are no longer needed by the browser,
    // because the calling page was left.
    return;
  }
  casper.echo('[warning] [remote] Failed to load resource: '
    + err.url, 'COMMENT');
  // noinspection JSUnresolvedVariable
  casper.echo('[warning] [remote] with message: '
    + err.errorString, 'COMMENT');

  // Show PHP errors that may be related
  var system = require('system');
  var script = system.env['MODULE_PATH'] + '/show_errors_from_watchdog.sh';
  var proc = require('child_process').spawn(script, '');

  proc.stdout.on('data', function log_output(data) {
    casper.echo(data, 'INFO');
  });
});

casper.on('page.error', function (msg) {
  var url = casper.getCurrentUrl();
  casper.echo('[warning] [DOM] ' + msg + ' (' + url + ')', 'WARNING');
});

casper.on('remote.message', function (msg) {
  var url = casper.getCurrentUrl();
  casper.echo('[info] [DOM] ' + msg + ' (' + url + ')', 'INFO');
});

casper.on('exit', function (status) {
  if (status) {
    const fs = require('fs');
    fs.write('new/optogram-DOM.html', casper.page.content);
    casper.capture('new/optogram.png');
  }
});

casper.options.waitTimeout = 15e3; // 15 seconds

var base_url = casper.cli.get('base-url');
casper.qdReplaceBaseUrl = function (url) {
  if (!base_url) {
    return url;
  }

  return url.replace('http://192.168.33.22', base_url)
    .replace('http://localhost', base_url);
};

if (base_url) {
  casper.setFilter('open.location', casper.qdReplaceBaseUrl);

  var waitForUrl = casper.waitForUrl;
  casper.waitForUrl = function (url) {
    if (typeof url === 'string') {
      arguments[0] = casper.qdReplaceBaseUrl(url);
    }
    return waitForUrl.apply(casper, arguments);
  };
}

casper.start('http://192.168.33.22');

// casper almost always freezes on the first attempt to load the login page
// from the intranet. This forces casper's PhantomJS webpage instance to
// load it again, which allows casper to continue its steps.
var timeout = window.setTimeout(function () {
  casper.page.open(base_url || 'http://192.168.33.22');
}, 5000);

casper.then(function () {
  window.clearTimeout(timeout);
});

casper.viewport(1024, 768);

casper.waitUntilVisible('#user-login-form');
casper.thenEvaluate(function () {
  // Prevent PhantomJS from remembering the previous test run:
  localStorage.clear();
});

module.exports = function (username, password) {
  var login = {
    name: username,
    pass: password,
  };

  casper.thenOpen('http://192.168.33.22');
  casper.then(function () {
    this.fill('#user-login-form', login);
  });

  casper.thenClick('#edit-submit');

  casper.waitFor(function () {
    return !this.getCurrentUrl().match(RegExp('/user/login'));
  }, null, function () {
    this.echo('[error] [casper] Failed to log in with '
      + login.name + ':' + login.pass + '!', 'ERROR');
    this.exit(1);
  });
};

casper.run();
