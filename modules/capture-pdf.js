/**
 * @param {string} name
 *   The last part of the file name without path or extension.
 * @param {string} url
 *   Where to download the pdf from
 * @param {object} black_out (optional), where to draw a black box, keyed by
 *   page number e.g.: {1: '105,1175 300,1210'};
 */
module.exports = function (name, url, black_out) {
  var cmd_had_error = false;

  casper.then(function () {
    var pdf_name = name + '.pdf';
    url = casper.qdReplaceBaseUrl(url);
    casper.download(url, pdf_name);
    run_cmd('mkdir', '-p new');

    // PDF conversion
    var test = this.cli.get(0).replace(/\.js$/, '');
    var target = 'new/' + casper.cli.get('install-profile') + '.' + test + '.' + name;
    var command = pdf_name + ' ' + target + ' -png';
    run_cmd('pdftoppm', command);
    casper.then(function () {
      if (cmd_had_error) {
        this.echo('[error] [pdftoppm] PDF conversion failed. PDF content:', 'ERROR');
        run_cmd('stat', pdf_name, true);
        run_cmd('cat', pdf_name, true);
      }
      else {
        this.echo('[info] [casper] Captured pdf pages ' + target, 'PARAMETER');
      }
    });


    black_out = black_out || {};
    for (var page in black_out) {
      var png = target + '-' + page + '.png';
      run_cmd('convert', [png, '-fill', 'black', '-draw', 'rectangle ' + black_out[page], png])
    }

    run_cmd('rm', pdf_name);
  });

  function run_cmd(binary, cmd, stdout) {
    casper.then(function () {
      cmd_had_error = false;
      var finished = false;
      var args = typeof cmd === 'string' ? cmd.split(' ') : cmd;
      var proc = require('child_process').spawn(binary, args);

      if (stdout) {
        proc.stdout.on("data", function (data) {
          casper.echo('[info] [' + binary + '] ' + JSON.stringify(data), 'PARAMETER');
        });
      }

      proc.stderr.on("data", function (data) {
        casper.echo('[error] [' + binary + '] ' + JSON.stringify(data), 'ERROR');
        cmd_had_error = true;
      });

      proc.on('exit', function () {
        finished = true;
      });

      casper.waitFor(function () {
        return finished;
      }, null, function () {
        casper.echo('[error] ' + binary + ' ' + cmd + ' did not finish in 60s', 'ERROR');
        casper.exit(1);
      }, 60e3);
    });
  }

};
