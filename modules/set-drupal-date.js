var already_enabled_module = false;

/**
 * Sets the time within Drupal. See also cy.clock() to set the time in Cypress.
 *
 * @param {number|string} timestamp
 *   Time to be set must be passed as a string, integer or float.
 *   If a string, pass it in this format: '2008-12-03 09:15pm'
 *   If a number, it needs to be seconds since 1970 (not milliseconds!).
 */
module.exports = function (timestamp) {
  if (!already_enabled_module) {
    require('./drush')('en datetime_testing --quiet');
    already_enabled_module = true;
  }
  if (typeof timestamp == 'string') {
    timestamp = '"' + timestamp + '"';
  }
  require('./drush')([
    'php:eval',
    '\\Drupal::time()->freezeTime(); ' +
    '\\Drupal::time()->setTime(' + timestamp + ');'
  ], false);
};
