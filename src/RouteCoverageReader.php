<?php

namespace Drupal\qd_screenshottests;

class RouteCoverageReader {

  public static function getRouteCoverage(): array {
    if (!is_dir(RouteCoverageWriter::TEST_COVERAGE_DIR))
      return [];

    $aRoutes = [];
    $oIter = new \RecursiveDirectoryIterator(RouteCoverageWriter::TEST_COVERAGE_DIR);
    foreach (new \RecursiveIteratorIterator($oIter) as $sFileName) {
      if (basename($sFileName) !== 'profiles')
        continue;
      $aInstallProfiles = json_decode(file_get_contents($sFileName) ?: '{}', TRUE) ?: [];
      foreach ($aInstallProfiles as $sProfile => $bCaptured) {
        $aInstallProfiles[$sProfile] = $bCaptured ?
          'Screenshot captured' :
          'Visited but no screenshot captured';
      }

      $sCurrentTest = basename(dirname($sFileName, 1));
      $sModule = basename(dirname($sFileName, 3));
      $sRoute = basename(dirname($sFileName, 4));
      $aRoutes[$sRoute]["$sModule/tests/$sCurrentTest"] = $aInstallProfiles;
    }
    ksort($aRoutes);
    foreach ($aRoutes as &$aRoute) {
      ksort($aRoute);
    }
    return $aRoutes;
  }
}
