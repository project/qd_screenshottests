module.exports = function (selector) {
  casper.thenEvaluate(function (selector) {
    var parent, i;
    var element = document.querySelector(selector);
    if (!element) {
      return;
    }

    while (parent = element.parentElement) {
      var children = parent.childNodes;
      for (i = 0; i < children.length; i++) {
        if (children[i].style && children[i] !== element) {
          children[i].style.display = 'none';
        }
      }
      element = parent;
    }
  }, selector);
};
