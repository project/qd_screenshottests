<?php

namespace Drupal\qd_screenshottests\WebDriver;

use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Firefox\FirefoxOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;

class FirefoxScreenshottestWebDriver extends FirefoxDriver {

  use ScreenshottestWebDriverTrait;

  public static function start(DesiredCapabilities $capabilities = NULL) {
    if (!$capabilities) {
      $capabilities = DesiredCapabilities::firefox();
      $capabilities->setCapability('acceptSslCerts', FALSE);
      $firefoxOptions = new FirefoxOptions();
      $firefoxOptions->addArguments(['-headless']);
      $capabilities->setCapability(FirefoxOptions::CAPABILITY, $firefoxOptions);
    }
    $driver = parent::start($capabilities);

    drupal_register_shutdown_function(static function () use ($driver) {
      $driver->quit();
    });

    return $driver;
  }

}
