module.exports = {
  "extends": "../../../core/.eslintrc.json",
  "globals": {
    "casper": true,
    "phantom": true
  },
  "rules": {
    "comma-dangle": 0,
    "no-cond-assign": 0,
    "no-var": 0,
    "vars-on-top": 0,
    "prefer-arrow-callback": 0,
    "func-names": 0,
    "camelcase": 0,
    "prefer-template": 0,
    "strict": 0
  }
};
