jQuery(function ($) {
  var screenshot = $('#screenshot');
  screenshot.click(function () {
    screenshot.attr('src', {
      diff: 'old',
      old: 'new',
      new: 'diff',
    }[screenshot.attr('src')]);
    screenshot.change();
  });

  screenshot.change(function () {
    $('u[selected]').removeAttr('selected');
    $('#' + screenshot.attr('src')).attr('selected', true);
  });

  screenshot.change();

  // Make the <u> tags above the image clickable:
  $('#old, #new, #diff').click(function () {
    screenshot.attr('src', $(this).attr('id'));
    screenshot.change();
  });

});
