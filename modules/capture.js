var names = {};
var count = 1;

/**
 *
 * @param {string} name
 *   The last part of the file name without path or extension.
 *   The final path is "tests/new/$install_profile.$js_test.$name.png".
 * @param {boolean} mask_dates (optional)
 *   If true, then the character X will be used in place of time-dependend
 *   numbers.
 * @param {object} viewport (optional) {
 *    top: 0,
 *    left: 0,
 *    width: 500,
 *    height: 500
 *  };
 */
module.exports = function (name, mask_dates, viewport) {
  casper.thenEvaluate(function (mask_dates) {
    if (!mask_dates) {
      return;
    }

    (function mask_node(node) {
      if (node.nodeType === document.TEXT_NODE) {
        var r = /\d\d?:\d\d/g;
        node.data = node.data.replace(r, 'XX:XX');
        r = /\d\d\.? (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\.? \d{4}/g;
        node.data = node.data.replace(r, 'DD Mth YYYY');
        r = /(Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\d\d\/\d\d\/\d{4})/g;
        node.data = node.data.replace(r, 'Wkd, $2');
        r = /\d\d([/.])\d\d\1\d{4}/g;
        node.data = node.data.replace(r, 'XX$1XX$1XXXX');
        r = /\d{4}-\d{2}-\d{2}/g;
        node.data = node.data.replace(r, 'XXXX-XX-XX');
      }
      else {
        var children = node.childNodes;
        for (var c = 0; c !== children.length; c++) {
          mask_node(children[c]);
        }
      }
    }(document));
  }, mask_dates);

  casper.then(function () {
    var test = this.cli.get(0).replace(/\.js$/, '');
    if (!name) {
      name = count++;
    }

    var target = 'new/' + casper.cli.get('install-profile')
      + '.' + test + '.' + name + '.png';
    this.capture(target, viewport);
    const fs = require('fs');
    fs.write(target + '-DOM.html', casper.page.content);

    this.echo('[info] [casper] Captured screenshot ' + target, 'PARAMETER');
    if (name in names) {
      this.log('Duplicate screenshot: ' + name, 'warning');
    }

    require('./drush')('--quiet qd_screenshottests:record-capture ' + this.getCurrentUrl());
    names[name] = true;
  });
};
