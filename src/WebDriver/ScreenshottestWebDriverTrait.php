<?php


namespace Drupal\qd_screenshottests\WebDriver;


use Facebook\WebDriver\WebDriverBy;

trait ScreenshottestWebDriverTrait {

  public function get($url) {
    return parent::get($this->getReplacedBasedUrl($url));
  }


  public function login($name, $password) {
    $this->get('http://192.168.33.22');
    $this->findElementByCss('#edit-name')->sendKeys($name);
    $this->findElementByCss('#edit-pass')->sendKeys($password);
    $this->click('#edit-submit');

    for ($i = 0; $i <= 60; $i++) {
      if (!str_contains($this->getCurrentURL(), '/user/login'))
        return;
      sleep(0.5);
    }
    throw new \RuntimeException('Did not redirect after login');
  }

  public function capture($name) {
    $profile = \Drupal::installProfile();
    $testName = str_replace('.selenium.php', '', $_SERVER['argv'][2]);

    $currentTestDir = \Drupal::state()->get('current_screenshottest');
    $modulePath = '';
    if (preg_match('/(.+)\/tests\//', $currentTestDir, $matches)) {
      $modulePath = \Drupal::moduleHandler()->getModule($matches[1])->getPath();
    }

    $target = "new/$profile.$testName.$name.png";
    echo '[info] [selenium] Captured screenshot ' . $target . "\n";
    $this->takeScreenshot(DRUPAL_ROOT . '/' . $modulePath . '/tests/' . $target);
  }


  public function click($selector) {
    $this->findElementByCss($selector)->click();
  }

  public function findElementByCss($selector) {
    return $this->findElement(WebDriverBy::cssSelector($selector));
  }

  private function getReplacedBasedUrl($url) {
    $baseUrl = getenv('QD_BASE_URL');
    if ($baseUrl && preg_match('/base-url=(.+)/', $baseUrl, $matches)) {
      $baseUrl = $matches[1];
      $url = str_replace('http://localhost', $baseUrl, $url);
      $url = str_replace('http://192.168.33.22', $baseUrl, $url);
    }
    return $url;
  }
}
