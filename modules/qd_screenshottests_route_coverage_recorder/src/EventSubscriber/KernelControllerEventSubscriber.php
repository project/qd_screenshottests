<?php

namespace Drupal\qd_screenshottests_route_coverage_recorder\EventSubscriber;

use Drupal\qd_screenshottests\RouteCoverageWriter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelControllerEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onRouteWasUsed',
    ];
  }

  public function onRouteWasUsed() {
    $sCurrentTest = \Drupal::state()->get('current_screenshottest');
    if (!$sCurrentTest) {
      // There is currently no test running.
      return;
    }

    // Do it once per test run
    if (!\Drupal::state()
      ->get('qd_screenshottests_route_coverage_recorder.entity_routes_set'))
      RouteCoverageWriter::writeAvailableEntityRoutes();

    // Documents which test uses which routes. This can later be picked up by
    // our Drush command.
    $sRoute = \Drupal::routeMatch()->getRouteName();
    RouteCoverageWriter::markAsUsed($sRoute);
  }

}
