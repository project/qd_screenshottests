<?php

namespace Drupal\qd_screenshottests\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\qd_screenshottests\Controller\ManageScreenshots;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class ReplaceNewByOldForm extends ConfirmFormBase {

  private $sTestsPath;

  private $sOldFile;

  private $sNewFile;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qd_screenshottests_replace_new_by_old';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to replace the current test result?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url(
      'qd_screenshottests.view_single_module_table',
      ['module_name' => $this->getRequest()->get('module_name')]);
  }

  private function fillFilePaths(): void {
    $oMS = new ManageScreenshots();
    $oRequest = $this->getRequest();
    $module = $oMS->getModuleByName($oRequest->get('module_name'));
    $screenshot = $oRequest->get('screenshot_name');
    $this->sTestsPath = DRUPAL_ROOT . '/' . $module->path;
    $this->sOldFile = $this->sTestsPath . '/old/' . $screenshot;
    $this->sNewFile = $this->sTestsPath . '/new/' . $screenshot;
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->fillFilePaths();
    $bSuccess = @file_put_contents($this->sNewFile, '', FILE_APPEND);
    if ($bSuccess === FALSE) {
      $form += self::buildChmodMarkup([$this->sTestsPath]);
      return $form;
    }


    $form['planned_command'] = [
      '#markup' => t(
        'This will execute <pre>cp \\<br>  :old \\<br>  :new</pre>', [
        ':old' => $this->sOldFile,
        ':new' => $this->sNewFile,
      ]),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->fillFilePaths();
    if (copy($this->sOldFile, $this->sNewFile)) {
      $this->messenger()->addStatus($this->t(
        'The new file was replaced by the old.'));
      $form_state->setRedirectUrl($this->getCancelUrl());
      return;
    }

    $this->messenger()->addError(t('Error while copying the file.'));
  }

  public static function buildChmodMarkup(array $aPaths): array {
    $form['hint'] = ['#markup' => t('To resolve permission problems, you need to execute the following command in Bash manually:')];
    $form['copy'] = ['#markup' => "<a id='copy' class='fa fa-copy'></a>"];
    $form['#attached'] = ['library' => ['qd_screenshottests/view_table']];
    $form['fixes'] = [
      '#type' => 'html_tag',
      '#tag' => 'pre',
      '#value' => 'chmod -R 0777 ' . join(' && \<br>chmod -R 0777 ', $aPaths),
      '#attributes' => ['id' => 'fixes'],
    ];
    return $form;
  }
}
