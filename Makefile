CASPERJS=$$(realpath casperjs-1.1.*)

.PHONY: modules bin

all: packages modules bin

packages: phantomjs-2.1.1-linux-x86_64 casperjs-1.1.4-2 geckodriver-v0.25.0

phantomjs-2.1.1-linux-x86_64:
	curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 | bzip2 -dc | tar -x

casperjs-1.1.4-2:
	curl -L https://github.com/casperjs/casperjs/archive/1.1.4-2.tar.gz | tar -zx

geckodriver-v0.25.0:
	curl -L https://github.com/mozilla/geckodriver/releases/download/v0.25.0/geckodriver-v0.25.0-linux64.tar.gz | tar -zx

bin: bin/screenshottest

bin/screenshottest: Makefile screenshottest.sh
	mkdir -p bin
	sed s:MODULE_PATH=.*:MODULE_PATH=$$(pwd): > bin/screenshottest < screenshottest.sh
	chmod a+x bin/*

install:
	apt-get install imagemagick poppler-utils firefox chromium-chromedriver -y
	pkill geckodriver || true
	mv geckodriver bin/
	cp bin/* /usr/local/bin
	cp bash_completion.sh /etc/bash_completion.d/screenshottest

modules: packages
	rm -f ${CASPERJS}/modules/modules
	ln -s -t ${CASPERJS}/modules $$(pwd)/modules

clean:
	git clean -dXf

realclean: clean
	rm -rf phantomjs* casperjs* geckodriver*
